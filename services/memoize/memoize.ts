/**
 * Memoize return
 */
export function memoize(target: any, key: string | symbol, descriptor: TypedPropertyDescriptor<any>) : TypedPropertyDescriptor<any> {

	let callback = descriptor.value;

	descriptor.value = function (descriptor: any) {
    return (<any>callback)[`@${descriptor.Number}`] || ((<any>callback)[`@${descriptor.Number}`] = callback.call(this, descriptor));
  };

  return descriptor;
  
}