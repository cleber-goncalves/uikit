import { memoize } from './memoize';

describe('memoize', function () {

  it('memoize function', function () {
    expect(memoize).toBeDefined();
  });

  it('memoize decorate', function () {

    var spy = 0;
    var descriptor = {
      value: function (a) { return a + spy };
    }

    descriptor = memoize(null, '', descriptor);

    expect(descriptor.value(1)).toBe(1);

    spy = 1;

    expect(descriptor.value(1)).toBe(1);
    
  });

});