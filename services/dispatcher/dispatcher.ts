export class Dispatcher {

  /**
   * Channel list
   */
  private static handlers: any = {};

  /**
   * Create a new reference
   */
  public static instance() {
    return new Dispatcher;
  }

  /**
   * Send to a channel
   */
  public emit(channel: string, data: any): Dispatcher {
    return this.get(channel).forEach(([predicate, context]) => { predicate.call(context, data); }), this;
  }

  /**
   * Gettter a channel
   */
  private get(channel: string): any[] {
    return Dispatcher.handlers[channel] = (Dispatcher.handlers[channel] || []);
  }

  /**
   * Off a listen to channel
   */
  public off(channel: string, predicate: Function): Dispatcher {
    return this.get(channel).filter(([f]) => !(f === predicate)), this;
  }
 
  /**
   * Listen to a channel
   */
  public on(channel: string, predicate: Function, context: any): Dispatcher {
    return this.get(channel).push([predicate, context]), this;
  }
  
}