import { Dispatcher } from './dispatcher';

describe('Dispatcher', function () {

  it('Dispatcher class', function () {
    expect(Dispatcher).toBeDefined();
  });

  it('Isntatnce method', function () {
    expect(Dispatcher.instance()).toEqual(new Dispatcher);
  });

  it('On, Off and Emit method', function () {
    function test(data) {
      expect(data).toBe(true);
    }
    Dispatcher.instance().on('test', test, this);
    Dispatcher.instance().emit('test', true);
    Dispatcher.instance().off('test', test);
    Dispatcher.instance().emit('test', false);
  });
  
});