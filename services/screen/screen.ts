import { Screen0, Screen1009, Screen1010, Screen1100 } from './';

export class Screen {

  private handlers: Object = {
    '0': Screen0,
    '1009': Screen1009,
    '1010': Screen1010,
    '1100': Screen1100
  };

  public static instance(): Screen {
    return new Screen;
  }

  public map(number: string): {} {
    return this.handlers[number] || {};
  }

}