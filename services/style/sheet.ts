export class Sheet {

  private properties: any[];
  private selector: string;

  public static builder(selector: string, properties: any[]) : Sheet {
    return new Sheet(selector, properties);
  }

  private constructor(selector: string, properties: any[]) {
    this.selector = selector;
    this.properties = properties;
  }

  private parsePropertiesToString(): string {
    return this.properties.map(([key, value, important]: any[]) => `${key}:${value} ${this.parseToImportant(important)};`).join('');
  }

  private parseToImportant(value: boolean): string {
    return value ? '!important' : '';
  }

  public toRule(): string {
    return `${this.selector} { ${this.parsePropertiesToString()} }`;
  }
  
}