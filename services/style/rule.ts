import { Sheet } from './sheet';

export class Rule {

  /**
   * Stylesheet
   */
  private style: CSSStyleSheet = document.head.appendChild(document.createElement('style'))['sheet'];

  /**
   * Create a new instance
   */
  public static instance() {
    return new Rule;
  }

  /**
   * Length of rules
   */
  private index(): number {
    return this.style.cssRules.length;
  }

  /**
   * Push a new rule
   */
  public insert(sheet: Sheet): Rule {
    return this.style.insertRule(sheet.toRule(), this.index()), this;
  }

}