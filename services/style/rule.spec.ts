import { Rule } from './rule';
import { Sheet } from './sheet';

describe('Rule', function () {

  it('Rule class', function () {
    expect(Rule).toBeDefined();
  });

  it('Isntatnce method', function () {
    expect(Rule.instance()).toEqual(new Rule);
  });

  it('insert method', function () {
    expect(Rule.instance().insert(Sheet.builder('.class', ['color', 'black']))).toEqual(new Rule);
  });
  
});