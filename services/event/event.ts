/**
* Created method override
*/
export function event(event: string, query: string): Function {
  return function (target: any, key: string | symbol, descriptor: TypedPropertyDescriptor<any>): TypedPropertyDescriptor<any> {

    let oldCreatedCallback = (target.createdCallback || function () { });
    let oldDetachedCallback = (target.detachedCallback || function () { });

    function action(e: Event) {
      (<Element>e.target).matches(query) && this[key](e);
    }

    target.createdCallback = function () {
      this.addEventListener(event, action.bind(this));
      oldCreatedCallback.call(this);
    };

    target.detachedCallback = function () {
      this.removeEventListener(event, action.bind(this));
      oldDetachedCallback.call(this);
    };

    return descriptor;

  };
}