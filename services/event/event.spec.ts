import { event } from './event';

describe('event', function () {

  it('event function', function () {
    expect(event).toBeDefined();
  });

  it('event decorate', function () {
    function target() {}
    event('', '')(target, '', {
      value: function() {
        expect(true).toBe(true);
      }
    });
    target.createdCallback();
  });

});