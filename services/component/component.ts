/**
 * Register HTML Element
 */
export function component(tagName: string) : Function {

  return function (target: any) : void {
    (<any>document).registerElement(tagName, {
      prototype: (<any>Object).assign(Object.create(HTMLElement.prototype), target.prototype)
    });
  };

}