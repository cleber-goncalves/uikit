/**
 * Detached method override
 */
export function detached(target : any, key: string | symbol, descriptor: TypedPropertyDescriptor<any>) : TypedPropertyDescriptor<any> {
  
  let oldDetachedCallback = (target.detachedCallback || function () {});

  target.detachedCallback = function () {
    descriptor.value.call(this);
    oldDetachedCallback.call(this);
  };

  return descriptor;

}