import { detached } from './detached';

describe('detached', function () {

  it('detached function', function () {
    expect(detached).toBeDefined();
  });

  it('detached decorate', function () {
    function target() {}
    detached(target, '', {
      value: function() {
        expect(true).toBe(true);
      }
    });
    target.detachedCallback();
  });

});