export function uuid(): string {
  return (Date.now() * Math.random()).toString(36);
}