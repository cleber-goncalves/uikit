export class Modal {

  /**
   * Node Element
   */
  private el: Element;

  /**
   * Create a new instance
   */
  public static instance(el: Element): Modal {
    return new Modal(el);
  }

  /**
   * constructor
   */
  constructor(el: Element) {
    this.el = el;
  }

  /**
   * Apply setting to bottom
   */
  private bottom(): Modal {
    (<any>this.el).style.left = `calc((100% - ${ this.width() }px) / 2)`;
    (<any>this.el).style.position = 'absolute';
    (<any>this.el).style.top = `calc(100% - ${ this.height() }px)`;
    return this;
  }

  /**
   * Apply setting to bottom and left
   */
  private bottomLeft(): Modal {
    (<any>this.el).style.left = `0`;
    (<any>this.el).style.position = 'absolute';
    (<any>this.el).style.top = `calc(100% - ${ this.height() }px)`;
    return this;
  }

  /**
   * Apply setting to bottom right
   */
  private bottomRight(): Modal {
    (<any>this.el).style.left = `calc(100% - ${ this.width() }px)`;
    (<any>this.el).style.position = 'absolute';
    (<any>this.el).style.top = `calc(100% - ${ this.height() }px)`;
    return this;
  }

  /**
   * Apply setting to close
   */
  public close(): Modal {
    (<any>this.el).style.left = 'auto';
    (<any>this.el).style.position = 'relative';
    (<any>this.el).style.top = 'auto';
    return this;
  }

  /**
   * Apply setting to middle
   */
  private middle(): Modal {
    (<any>this.el).style.left = `calc((100% - ${ this.width() }px) / 2)`;
    (<any>this.el).style.position = 'absolute';
    (<any>this.el).style.top = `calc((100% - ${ this.height() }px) / 2)`;
    return this;
  }

  /**
   * Apply setting to middle and left
   */
  private middleLeft(): Modal {
    (<any>this.el).style.left = '0';
    (<any>this.el).style.position = 'absolute';
    (<any>this.el).style.top = `calc((100% - ${ this.height() }px) / 2)`;
    return this;
  }

  /**
   * Apply setting to middle right
   */
  private middleRight(): Modal {
    (<any>this.el).style.left = `calc(100% - ${ this.width() }px)`;
    (<any>this.el).style.position = 'absolute';
    (<any>this.el).style.top = `calc((100% - ${ this.height() }px) / 2)`;
    return this;
  }

  /**
   * Apply setting to top
   */
  private top(): Modal {
    (<any>this.el).style.left = `calc((100% - ${ this.width() }px) / 2)`;
    (<any>this.el).style.position = 'absolute';
    (<any>this.el).style.top = '0';
    return this;
  }

  /**
   * Apply setting to top and left
   */
  private topLeft(): Modal {
    (<any>this.el).style.left = '0';
    (<any>this.el).style.position = 'absolute';
    (<any>this.el).style.top = '0';
    return this;
  }

  /**
   * Apply setting to top and right
   */
  private topRight(): Modal {
    (<any>this.el).style.left = `calc(100% - ${ this.width() }px)`;
    (<any>this.el).style.position = 'absolute';
    (<any>this.el).style.top = '0';
    return this;
  }

  /**
   * Apply setting to top, middle or bottom
   */
  public open(coordinate: string): Modal {
    return (this[coordinate] || function () {}).call(this);
  }

  /**
   * Element height return
   */
  private height(): number {
    return ~~this.style().height.slice(0, -2);
  }

  /**
   * Element style return
   */
  private style(): CSSStyleDeclaration {
    return document.defaultView.getComputedStyle(this.el);
  }

  /**
   * Element width return
   */
  private width(): number {
    return ~~this.style().width.slice(0, -2);
  }

}