import { Modal } from './modal';

describe('Modal', function () {

  it('Modal class', function () {
    expect(Modal).toBeDefined();
  });

  it('Isntatnce method', function () {
    expect(Modal.instance(document.body)).toEqual(new Modal(document.body));
  });

  it('Close method', function () {
    expect(Modal.instance(document.body).close()).toEqual(new Modal(document.body));
  });

  it('Open method', function () {    
    let modal = Modal.instance(document.body);
    let compare = new Modal(document.body);

    expect(modal.open('middle')).toEqual(compare);
    expect(modal.open('middleLeft')).toEqual(compare);
    expect(modal.open('middleRight')).toEqual(compare);
    expect(modal.open('top')).toEqual(compare);
    expect(modal.open('topLeft')).toEqual(compare);
    expect(modal.open('topRight')).toEqual(compare);
    expect(modal.open('bottom')).toEqual(compare);
    expect(modal.open('bottomLeft')).toEqual(compare);
    expect(modal.open('bottomRight')).toEqual(compare);
  });

});