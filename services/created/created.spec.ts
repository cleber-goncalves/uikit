import { created } from './created';

describe('created', function () {

  it('created function', function () {
    expect(created).toBeDefined();
  });

  it('created decorate', function () {
    function target() {}
    created(target, '', {
      value: function() {
        expect(true).toBe(true);
      }
    });
    target.createdCallback();
  });

});