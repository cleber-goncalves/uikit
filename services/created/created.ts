/**
 * Created method override
 */
export function created(target : any, key: string | symbol, descriptor: TypedPropertyDescriptor<any>) : TypedPropertyDescriptor<any> {

  let oldCreatedCallbadk = (target.createdCallback || function () {});

  target.createdCallback = function () {
    descriptor.value.call(this);
    oldCreatedCallbadk.call(this);
  };

  return descriptor;

}