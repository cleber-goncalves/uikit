import { Store } from './store';

describe('Store', function () {

  it('Store class', function () {
    expect(Store).toBeDefined();
  });

  it('Isntatnce method', function () {
    expect(Store.instance()).toEqual(new Store);
  });

  it('Set and Get method', function () {
    Store.instance().set('a', 1);
    expect(Store.instance().get('a')).toBe(1);
  });

  it('Has method', function () {
    Store.instance().set('test', { a: 10 });
    expect(Store.instance().has('test')).toBe(true);
    expect(Store.instance().has('x')).toBe(false);
  });

  it('Remove method', function () {
    Store.instance().set('test', { a: 10 });
    expect(Store.instance().has('test')).toBe(true);
    Store.instance().remove('test');
    expect(Store.instance().has('test')).toBe(false);
  });

  it('Clear method', function () {
    Store.instance().set('a', { a: 1 });
    Store.instance().set('b', { b: 1 });
    expect(Store.instance().has('a')).toBe(true);
    expect(Store.instance().has('b')).toBe(true);
    Store.instance().clear();
    expect(Store.instance().has('a')).toBe(false);
    expect(Store.instance().has('b')).toBe(false);
  });
  
});