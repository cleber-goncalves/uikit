export class Store {

	/**
	 * Data
	 */
	private static data: any = {};

	/**
	 * Create a new reference
	 */
	public static instance(): Store {
		return new Store;
	}

	/**
	 * Removes all keys
	 */	
	public clear(): Store {
		return (Store.data = {}), this;
	}

	/**
	 * Clone the data
	 */
	private copy(data: any): any {
		return JSON.parse(JSON.stringify(data || {}));
	}

	/**
	 * Getter a key
	 */
	public get(name: string): any {
		return this.copy(Store.data[name]);
	}

	/**
	 * Checks a key
	 */
	public has(name: string): boolean {
		return !/undefined/.test(Store.data[name]);
	}
	
	/**
	 * Delete a key
	 */
	public remove(name: string): Store {
		return (Store.data[name] = undefined), this;
	}

	/**
	 * Add or change the velue of a key
	 */
	public set(name: string, data: any): Store {
		return (Store.data[name] = this.copy(data)), this;
	}

}