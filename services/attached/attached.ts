/**
 * Attached method override
 */
 export function attached(target : any, key: string | symbol, descriptor: TypedPropertyDescriptor<any>) : TypedPropertyDescriptor<any> {

  let oldAttachedCallbadk = (target.attachedCallback || function () {});

  target.attachedCallback = function () {
    descriptor.value.call(this);
    oldAttachedCallbadk.call(this);
  };

  return descriptor;

}