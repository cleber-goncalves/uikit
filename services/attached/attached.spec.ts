import { attached } from './attached';

describe('attached', function () {

  it('attached function', function () {
    expect(attached).toBeDefined();
  });

  it('attached decorate', function () {
    function target() {}
    attached(target, '', {
      value: function() {
        expect(true).toBe(true);
      }
    });
    target.attachedCallback();
  });

});