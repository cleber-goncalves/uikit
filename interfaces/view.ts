export interface View {
  destroy(): void;
  render(): void;
}