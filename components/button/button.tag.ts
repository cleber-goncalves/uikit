/**
 * Get tag of button
 */
export function buttonTag(descriptor?: any) : string {
  return `
    <rdi-button
      channel="${ descriptor.channel || '' }"
      h="${ descriptor.h || '' }"
      modifier="${ descriptor.modifier || '' }"
      number="${ descriptor.number || '' }"
      type="${ descriptor.type || ''}"
      v="${ descriptor.v || '' }"
      value="${ descriptor.value || '' }"
      uuid="${ descriptor.uuid || '' }"
    >
    </rdi-button>
  `;
}