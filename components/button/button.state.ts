export class ButtonState {

  private el: Element;

  /**
   * Channel of Dispatcher.emit
   */
  get channel(): string {
    return this.el.getAttribute('channel') || '';
  }

  /**
   * Width
   */
  get h(): string {
    return this.el.getAttribute('h') || '0';
  }

  /**
   * Class modifier
   */
  get modifier(): string {
    return this.el.getAttribute('modifier') ? `-${ this.el.getAttribute('modifier') }` : '';
  }

  /**
   * Button number
   */
  get number(): string {
    return this.el.getAttribute('number') || '0';
  }

  /**
   * Screen Number type
   */
  get type(): string {
    return this.el.getAttribute('type') || '0';
  }

  /**
   * Unique User identifier
   */
  get uuid(): string {
    return this.el.getAttribute('uuid') || '';
  }

  /**
   * Height
   */
  get v(): string {
    return this.el.getAttribute('v') || '0';
  }

  /**
   * Text value
   */
  get value(): string {
    return this.el.getAttribute('value') || '';
  }

  /**
   * Constructor
   */
  constructor(el: Element) {
    this.el = el;
  }

  /**
   * Create a new instance
   */
  public static instance(el: Element): ButtonState {
    return new ButtonState(el);
  }
  
}