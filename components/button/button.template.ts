import { ButtonState } from './';

/**
 * Get template of button
 */
export function buttonTemplate(state: ButtonState): string {
  return `
    <div class="button${ state.modifier }">
      <span class="button_text">${ state.value }</span>
    </div>
  `;
}