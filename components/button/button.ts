import { attached, component, created, Dispatcher, event, Screen } from './../../services';
import { Dom } from './../../interfaces';
import { ButtonState, buttonTag, buttonTemplate } from './';

@component('rdi-button')
export class Button implements Dom {

  /**
   * State reference
   */
  private state: ButtonState;

  /**
   * Dispatcher reference
   */
  private dispatcher: Dispatcher;

  /**
   * Screen map
   */
  private screen: any;  

  /**
   * Click event
   */
  @event('click', 'span.button_text')
  public action(): Button {
    return this.dispatcher.emit(this.state.channel, this.state.uuid), this;
  }

  /**
   * calculation position
   */
  private calc(): Button {
    (<any>this).style.height = this.screen.v(this.state.v);
    (<any>this).style.left = this.screen[this.state.number][0];
    (<any>this).style.position = 'absolute';
    (<any>this).style.top = this.screen[this.state.number][1];
    (<any>this).style.width = this.screen.h(this.state.h);
    return this;
  }

  /**
   * Create a new reference
   */
  public static init(): Button {
    return new Button;
  }

  /**
   * Dependencies builder
   */
  private build(): Button {
    this.dispatcher = Dispatcher.instance(),
    this.state = ButtonState.instance(<any>this),
    this.screen = Screen.instance().map(this.state.type);
    return this;
  }
  
  /**
   * Render HTML template
   */
  @attached
  private render(): Button {
    return (<any>this).innerHTML = buttonTemplate(this.state), this;
  }

  /**
   * Constructor
   */
  @created
  private start(): Button {
    return this.build().calc();
  }

  /**
   * Render HTML tag
   */
  public toHTML(descriptor?: any): string {
    return buttonTag(descriptor);
  }

}