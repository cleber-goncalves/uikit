import { Button } from './../'
import { ActionState } from './';

/**
 * Get template of button
 */
export function actionTemplate(state: ActionState): string {
  return `${ Button.init().toHTML({
    channel: state.token,
    h: state.h,
    modify: state.modifier,
    number: state.number,
    screen: state.screen,
    type: state.type,
    v: state.v,
    value: state.value })
  }`;
}