import { attached, component, created, detached, Dispatcher,  event } from './../../services';
import { Dom } from './../../interfaces';
import { ActionState, actionTag, actionTemplate } from './';

@component('rdi-action')
export class Action implements Dom {

  /**
   * Dispatcher reference
   */
  private dispatcher: Dispatcher;

  /**
   * State reference
   */
  private state: ActionState;

  /**
   * Create a new reference
   */
  public static init(): Action {
    return new Action;
  }

  /**
   * Custom constructor
   */
  @created
  private start(): Action {
    return this.build().subscribe();
  }

  /**
   * Dependencies builder
   */
  private build(): Action {
    this.dispatcher = Dispatcher.instance(),
    this.state = ActionState.instance(<any>this);
    return this;
  }

  /**
   * Unsubscribe a channel
   */
  @detached
  private unsubscribe(): Action {
    return this.dispatcher.off(this.state.token, this.notify), this;
  }

  /**
   * Subscribe a channel
   */
  private subscribe(): Action {
    return this.dispatcher.on(this.state.token, this.notify, this), this;
  }

  /**
   * Notify backend
   */
  private notify(): Action {
    return (<any>window).$.post(`./api/${ this.state.channel }`, { ScreenNumber: this.state.screen, ButtonNumber: this.state.uuid, Command: this.state.command }), this;
  }
  
  /**
   * Render HTML template
   */
  @attached
  private render(): Action {
    return (<any>this).innerHTML = actionTemplate(this.state), this;
  }

  /**
   * Render HTML tag
   */
  public toHTML(descriptor?: any): string {
    return actionTag(descriptor);
  }

}