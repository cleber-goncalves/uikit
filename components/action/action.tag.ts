/**
 * Get tag of button
 */
export function actionTag(descriptor?: any) : string {
  return `
    <rdi-action
      channel="${ descriptor.channel || '' }"
      command="${ descriptor.command || '' }"
      h="${ descriptor.h || '' }"
      modifier="${ descriptor.modifier || '' }"
      number="${ descriptor.number || '' }"
      screen="${ descriptor.screen || ''}"
      type="${ descriptor.type || ''}"
      v="${ descriptor.v || '' }"
      value="${ descriptor.value || '' }"
      uuid="${ descriptor.uuid || '' }"
    >
    </rdi-action>
  `;
}