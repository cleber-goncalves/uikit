import { uuid } from './../../services';

export class ActionState {

  private el: Element;

  /**
   * Unique user identifier
   */
  private __token__: string;

  /**
   * Channel of Dispatcher.emit
   */
  get channel(): string {
    return this.el.getAttribute('channel');
  }

  /**
   * Comand
   */
  get command(): string {
    return this.el.getAttribute('command');
  }

  /**
   * Width
   */
  get h(): string {
    return this.el.getAttribute('h');
  }

  /**
   * Class modifier
   */
  get modifier(): string {
    return this.el.getAttribute('modifier') ? `-${ this.el.getAttribute('modifier') }` : '';
  }

  /**
   * Button number
   */
  get number(): string {
    return this.el.getAttribute('number');
  }

  /**
   * Screen Number
   */
  get screen(): string {
    return this.el.getAttribute('screen');
  }

  /**
   * Secret channel
   */
  get token(): string {
    return this.__token__;
  }

  /**
   * Screen Number type
   */
  get type(): string {
    return this.el.getAttribute('type');
  }

  /**
   * Unique User identifier
   */
  get uuid(): string {
    return this.el.getAttribute('uuid');
  }

  /**
   * Height
   */
  get v(): string {
    return this.el.getAttribute('v');
  }

  /**
   * Text value
   */
  get value(): string {
    return this.el.getAttribute('value');
  }

  /**
   * Constructor
   */
  constructor(el: Element) {
    this.el = el,
    this.__token__ = uuid();
  }

  /**
   * Create a new instance
   */
  public static instance(el: Element): ActionState {
    return new ActionState(el);
  }
  
}