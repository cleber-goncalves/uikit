import { attached, component, created, Dispatcher, event, Modal } from './../../services';
import { Dom } from './../../interfaces';
import { KeyBoardState, KeyBoardTag, KeyBoardTemplate } from './';

@component('rdi-keyboard')
export class KeyBoard implements Dom {

  private dispatcher: Dispatcher;

  private modal: Modal;

  private state: KeyBoardState;

  public static init(): KeyBoard {
    return new KeyBoard;
  }

  private build(): KeyBoard {
    this.dispatcher = Dispatcher.instance(),
    this.state = KeyBoardState.instance(<any>this),
    this.modal = Modal.instance(<any>this);
    return this;
  }

  private close(): KeyBoard {
    return this.modal.close(), this.hide();
  }

  private format(): KeyBoard {
    (<any>this).style.height = '40vh';
    (<any>this).style.zIndex = '1000';
    (<any>this).style.width = '100%';
    return this;
  }

  private hide(): KeyBoard {
    return (<any>this).style.display = 'none', this;
  }

  private off(): KeyBoard {
    this.dispatcher.off('keyboard:close', this.close)
                   .off('keyboard:open', this.open);
    return this;
  }

  private on(): KeyBoard {
    this.dispatcher.on('keyboard:close', this.close, this)
                   .on('keyboard:open', this.open, this);
    return this;
  }

  private open(): KeyBoard {
    return this.modal.open('bottomLeft'), this.show();
  }

  @attached
  private render(): KeyBoard {
    return (<any>this).innerHTML = KeyBoardTemplate(this.state), this;
  }

  private show(): KeyBoard {
    return (<any>this).style.display = 'block', this;
  }

  @created
  private start(): KeyBoard {
    return this.build().on().format().hide();
  }

  toHTML(descriptor?: any) {
    return KeyBoardTag(descriptor);
  }
  
}