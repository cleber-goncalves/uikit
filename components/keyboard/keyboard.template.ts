import { Button } from './../../components';
import { KeyBoardState } from './';

export function KeyBoardTemplate(state: KeyBoardState): string {
  return state.buttons.map(b => Button.init().toHTML({ channel: state.channel, h: '', number: '', type: state.type, v: '', value: '' })).join('');
}