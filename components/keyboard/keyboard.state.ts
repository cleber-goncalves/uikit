import { Store } from './../../services';

export class KeyBoardState {

  private el: Element;

  private store: Store;

  /**
   * List buttons
   */
  get buttons(): any[] {
    // return this.store.get('');
    return [];
  }

  /**
   * Channel of Dispatcher.emit
   */
  get channel(): string {
    return 'keyboard:press';
  }

  /**
   * Screen Number type
   */
  get type(): string {
    return '1010';
  }

  /**
   * Constructor
   */
  constructor(el: Element) {
    this.el = el,
    this.store = Store.instance();
  }

  /**
   * Create a new instance
   */
  public static instance(el: Element): KeyBoardState {
    return new KeyBoardState(el);
  }
  
}